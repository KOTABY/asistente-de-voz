se importan los paquetes nesesarios
===================================

### spech_recognition paquete para reconocer la voz
import speech_recognition as sr

### paquete que reproducira los audios (solo tipo wav)
import winsound

### paquete que abrira las aplicaciones de windows
import os

el programa
===========

### variable que uso para acortar sr.Recognizer()
r = sr.Recognizer()

### variable que da fin al bucle while
salida = "hola"

### inicio del bucle (donde esta todo el codigo del programa)
while salida != "salir":

    with sr.Microphone() as source:
        winsound.PlaySound("correcto", winsound.SND_ASYNC)
        print("el micro esta escuchando...")
        audio = r.listen(source)
        text = r.recognize_google(audio)
        list = []
        list.append(text)
    print(list)
    # text = "hola"

    if text == "hola":
        print("\nEstas son algunas de las cosas que puede hacer:")
        print("\n Abrir programas (primero di abrir lista de programas)\n")
        timedelay = input("presiona enter")
        print(text)
    if "programas" in list:
        print("para abrir notepad di: (abrir notepad")
        print("para abrir el explorador di: (abrir google")
        print(text)

    if list[0] == "abrir Notepad":
        os.system("notepad")

    if list[0] == "abrir Google":
        os.system("start chrome")
    else:
        winsound.PlaySound("error", winsound.SND_FILENAME)
        print("\nEsto no a funcionado")
        timedelay = input("si quieres volver a intentarlo clica enter: ")