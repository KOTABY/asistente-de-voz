ASISTENTE DE VOZ🎤
================
es un progrma que e montado para aprender i entender el funcionamiento del reconocimiento de voz con python i otros paquetes

los paquetes que nesesitas tener instalados son:🎁
------------------------------------------------

### pip v-20.1.1

para instalarlo deves descargar este archivo
https://bootstrap.pypa.io/get-pip.py

despues en la terminal ejecutas este comando (en la carpeta donde esta el archivo descargado)

python get-pip.py

### speech_recognition v-3.8.1

pip install SpeechRecognition

### winsound v-0.2.11

primero descarga este archivo

PyAudio‑0.2.11‑cp37‑cp37m‑win_amd64.whl

lo encontraras en esta pagina
https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyaudio

despues ejecuta esto en la terminal (en la carpeta donde esta el archivo descargado)

pip install PyAudio-0.2.11-cp37-cp37m-win_amd64.whl


documentacion https://docs.python.org/3/library/winsound.html

### mysql-connector v-2.2.9

ejecuta este comando en la terminal

pip install mysql-connector

### sobre el codigo del programa
consulta el archivo (funcionamiento.md)